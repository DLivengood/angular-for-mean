import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from '../customer';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  customer: Customer = null;
  customers: Customer[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  clean(): void {
    this.customer = null;
    this.customers = [];
  }

  getCustomer(cid: string): void {
    this.http.get<Customer>('http://localhost:3000/api/customers/' + cid)
      .subscribe(data => {
        if (data != null) {
          this.customer = data;
        }
      });
  }

  getCustomers(): void {
    this.http.get<Customer[]>('http://localhost:3000/api/customers/')
      .subscribe(data => {
        if (data != null) {
          this.customers = data;
        }
      });
  }

  deleteCustomer(cid: string): void {
    this.http.delete<Customer>('http://localhost:3000/api/customers/' + cid)
      .subscribe(data => {
        if (data != null) {
          this.customer = data;
        }
      });
  }
}
